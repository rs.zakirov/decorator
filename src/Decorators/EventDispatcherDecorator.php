<?php

namespace App\Decorators;

use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;

class EventDispatcherDecorator
{
    private LoggerInterface $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function dispatch(EventDispatcherInterface $dispatcher, object $event)
    {
        $dispatcher->dispatch($event);
        $this->logger->info('Mess with info event object');
    }
}